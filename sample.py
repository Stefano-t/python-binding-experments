import random
import numpy as np
from math import floor


def plain_sample(probs: np.ndarray) -> int:
    cum_sum = probs.cumsum()
    if not np.isclose(cum_sum[-1], 1.0):
        raise ValueError("input doesn't sum up to 1!")

    prob = random.random()

    for idx in range(len(cum_sum)):
        if prob < cum_sum[idx]:
            break

    return idx


def bidim(probs: np.ndarray) -> int:
    if not np.isclose(probs.sum(), 1.0):
        raise ValueError("input doesn't sum up to 1!")

    box = probs / probs.max()

    while True:
        idx = floor(random.random() * len(probs))
        if random.random() <= box[idx]:
            return idx


def roulette_wheel(probs: np.ndarray) -> int:
    cumsum = probs.cumsum()
    if not np.isclose(cumsum[-1], 1.0):
        raise ValueError("input doesn't sum up to 1!")

    prob = random.random()
    return np.searchsorted(cumsum, prob)
