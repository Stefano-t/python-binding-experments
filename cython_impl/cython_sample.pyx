import random
import numpy as np
from math import floor

# Import compile-time information
cimport numpy as np

# Side effects to init python C interface
np.import_array()

# Data type for our array
DTYPE = np.float_

# Type definition of the underlying object
ctypedef np.float_t DTYPE_t


def plain_sample(np.ndarray[DTYPE_t, ndim=1] probs):
    assert probs.dtype == DTYPE
    cdef np.ndarray[DTYPE_t, ndim=1] cum_sum = probs.cumsum()
    if not np.isclose(cum_sum[-1], 1.0):
        raise ValueError("doesn't sum up to 1.0")

    cdef float prob = random.random()
    for idx in range(len(cum_sum)):
        if prob < cum_sum[idx]:
            break

    return idx

def bidim(np.ndarray[DTYPE_t, ndim=1] probs):
    assert probs.dtype == DTYPE

    if not np.isclose(probs.sum(), 1.0):
        raise ValueError("doesn't sum up to 1.0")

    cdef np.ndarray[DTYPE_t, ndim=1] box = probs / probs.max()
    cdef int arr_len = len(probs)
    cdef float prob
    cdef int idx

    while True:
        idx = random.randint(0, arr_len-1)
        prob = random.random()
        if prob < box[idx]:
            return idx


def roulette_wheel(np.ndarray[DTYPE_t, ndim=1] probs):
    cdef np.ndarray[DTYPE_t, ndim=1] cumsum = probs.cumsum()
    if not np.isclose(cumsum[-1], 1.0):
        raise ValueError("input doesn't sum up to 1!")

    cdef float prob = random.random()
    return np.searchsorted(cumsum, prob)


cdef unsigned int MAX_VALUE = 2 ** 32 - 1

cdef class RNG:

    cdef unsigned int seed

    def __init__(self, unsigned int seed):
         self.seed = seed

    cpdef next_int(self):
        cdef unsigned int old = self.seed
        old ^= (old << 13)
        old ^= (old >> 7)
        old ^= (old << 17)
        self.seed = old
        return old

    cpdef next_float(self):
        cdef unsigned int random_int = self.next_int()
        return <float>random_int / MAX_VALUE


GLOBAL_RNG = RNG(1234)


def bidim_rng(np.ndarray[DTYPE_t, ndim=1] probs):
    assert probs.dtype == DTYPE

    if not np.isclose(probs.sum(), 1.0):
        raise ValueError("doesn't sum up to 1.0")

    cdef np.ndarray[DTYPE_t, ndim=1] box = probs / probs.max()
    cdef int arr_len = len(probs)
    cdef float prob
    cdef int idx

    while True:
        idx = GLOBAL_RNG.next_int() % arr_len
        prob = random.random()
        if prob < box[idx]:
            return idx


def binary_insert(np.ndarray[DTYPE_t, ndim=1] arr, float target):
    cdef int start = 0
    cdef int end = len(arr) - 1
    cdef int mid = (end + start) // 2

    while start < end:
        if arr[mid] < target:
            start = mid + 1
        elif arr[mid] > target:
            end = mid - 1
        else:
            return mid
        mid = (end + start) // 2

    return mid


def roulette_wheel_custom_search(np.ndarray[DTYPE_t, ndim=1] probs):
    cdef np.ndarray[DTYPE_t, ndim=1] cumsum = probs.cumsum()
    if not np.isclose(cumsum[-1], 1.0):
        raise ValueError("input doesn't sum up to 1!")

    cdef float prob = random.random()
    return binary_insert(cumsum, prob)
