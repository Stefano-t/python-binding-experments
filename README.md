Little experiment that benchmarks different implementations of three algorithms to sample a random number given a probability distribution. The experiment compares how fast algorithms are when implemented using:

- Python and numpy
- Cython
- Numba
- Rust (+ Python bindings)

## Create the environment

To create the environment, run the following code inside the project's root directory:

```bash
$ python3 -m venv .env
$ source .env/bin/activate
$ pip install -r requirements.txt
```


## Benchmarks

To reproduce the benchmarks, load `.env` environment running `source .env/bin/activate` and then execute `benchmark.ipynb` through `jupyter notebook`.

## Resources

- https://www.keithschwarz.com/darts-dice-coins/
- https://cython.readthedocs.io/en/latest/index.html
- https://github.com/PyO3/pyo3
