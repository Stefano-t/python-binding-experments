use pyo3::prelude::*;
use pyo3::{pymodule, types::PyModule, PyResult, Python};
use numpy::ndarray::{Axis, Array1, ArrayView1, ArrayBase, ViewRepr, Dim};
use numpy::{IntoPyArray, PyArray1, PyReadonlyArray1};


#[derive(Debug)]
#[pyclass]
/// Random number generator based on xorshift algorithm
struct RandomNumberGenerator(u32);

const DIVISOR: f64 = 1f64 / (u32::MAX as f64);

static mut GLOBAL_RANDOM: RandomNumberGenerator = RandomNumberGenerator(123); // @TODO: change to current time

#[pymethods]
impl RandomNumberGenerator {
    #[new]
    fn new(seed: u32) -> Self {
        RandomNumberGenerator(seed)
    }

    /// Return next u32
    fn next_int(&mut self) -> u32 {
        let mut state = self.0;
        state ^= state << 13;
        state ^= state >> 17;
        state ^= state << 5;
        self.0 = state;
        state
    }

    /// Return next f64
    fn next_float(&mut self) -> f64 {
        let rn = self.next_int() as f64;
        rn * DIVISOR
    }

}

/// Formats the sum of two numbers as string.
#[pyfunction]
fn sum_as_string(a: usize, b: usize) -> PyResult<String> {
    Ok((a + b).to_string())
}

// @NOTE: quite slow! We should get rid off 'to_owned'
/// Cumulative sum of a numpy array
fn cumsum(a: ArrayView1<'_, f64>) -> Array1<f64> {
    let mut copy = a.to_owned();
    copy.accumulate_axis_inplace(Axis(0), |&prev, curr| *curr += prev);
    copy
}

#[inline]
/// Checks whenever `a` and `b` have a relative difference of `dp`.
fn approx_equal(a: f64, b: f64, dp: u8) -> bool {
    let p = 10f64.powi(-(dp as i32));
    (a-b).abs() < p
}

/// Extracts an index between 0 and len(a) according to the probability
/// distribution in `a` with a linear scanning of the vector.
fn plain_sample(a: ArrayView1<'_, f64>) -> usize {
    let cs = cumsum(a);
    if !approx_equal(*(cs.last().unwrap()), 1.0f64, 12) {
        panic!("Values don't sum up to 1");
    }
    let mut f: usize = 0;
    unsafe {
        let extraction = GLOBAL_RANDOM.next_float();
        for (idx, &v) in cs.iter().enumerate() {
            if extraction < v {
                f = idx;
                break;
            }
        }
    }
    f
}

// @NOTE: slow AF!
/// Extracts an index between 0 and len(a) according to the probability
/// distribution in `a` with a bidimensional extraction.
fn bidim_sample(a: ArrayBase<ViewRepr<&f64>, Dim<[usize; 1]>>) -> usize {
    let cs = cumsum(a);
    if !approx_equal(*(cs.last().unwrap()), 1.0f64, 12) {
        panic!("Values don't sum up to 1");
    }
    let max = 1.0 / a.iter().max_by(|a, b| a.partial_cmp(b).unwrap()).unwrap();
    let box_values = &a * max;

    let length: f64 = a.len() as f64;
    let mut idx: usize;
    let mut extraction: f64;

    loop {
        unsafe {
            idx        = (GLOBAL_RANDOM.next_float() * length) as usize;
            extraction = GLOBAL_RANDOM.next_float();
        }
        if extraction <= box_values[idx] {
            break;
        }
    }
    idx
}

/// Extracts an index between 0 and len(a) according to the probability
/// distribution in `a` with a roulette wheel strategy.b
fn roulette_wheel(a: ArrayBase<ViewRepr<&f64>, Dim<[usize; 1]>>) -> usize {
    let cs = cumsum(a);
    if !approx_equal(*(cs.last().unwrap()), 1.0f64, 12) {
        panic!("Values don't sum up to 1");
    }

    let extraction: f64;
    unsafe {
        extraction = GLOBAL_RANDOM.next_float();
    }
    binary_search(a, extraction)
}

fn binary_search(a: ArrayBase<ViewRepr<&f64>, Dim<[usize; 1]>>, extraction: f64) -> usize {
    let mut start: usize = 0;
    let mut end:   usize = 0;
    let mut mid:   usize = 0;

    while start < end {
        if a[mid] < extraction {
            start = mid + 1;
        } else if a[mid] > extraction {
            end = mid - 1;
        } else {
            return mid;
        }
        mid = (end + start) / 2;
    }

    mid
}

/// A Python module implemented in Rust.
/// Make sure to match the name of the module specified inside Cargo.toml
#[pymodule]
fn rust_impl(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(sum_as_string, m)?)?;
    m.add_class::<RandomNumberGenerator>()?;
    // m.add_function(wrap_pyfunction!(plain_sample, m)?)?;


    #[pyfn(m)]
    #[pyo3(name = "cumsum")]
    fn cumsum_py<'py>(
        py: Python<'py>,
        a: PyReadonlyArray1<f64>,
    ) -> &'py PyArray1<f64> {
        let a = a.as_array();
        cumsum(a).into_pyarray(py)
    }

    #[pyfn(m)]
    #[pyo3(name = "plain_sample")]
    fn plain_sample_py<'py>(
        _py: Python<'py>,
        a: PyReadonlyArray1<f64>,
    ) -> usize {
        let a = a.as_array();
        plain_sample(a)
    }

    #[pyfn(m)]
    #[pyo3(name = "bidim_sample")]
    fn bidim_sample_py<'py>(
        _py: Python<'py>,
        a: PyReadonlyArray1<f64>,
    ) -> usize {
        bidim_sample(a.as_array())
    }

    #[pyfn(m)]
    #[pyo3(name = "roulette_wheel")]
    fn roulette_wheel_py<'py>(
        _py: Python<'py>,
        a: PyReadonlyArray1<f64>,
    ) -> usize {
        roulette_wheel(a.as_array())
    }

    #[pyfn(m)]
    #[pyo3(name = "binary_search")]
    fn binary_seach_py<'py>(
        _py: Python<'py>,
        a: PyReadonlyArray1<f64>,
        target: f64,
    ) -> usize {
        binary_search(a.as_array(), target)
    }

    Ok(())
}
