## How to compile this module

First of all, make sure `.env` is loaded. We're using `maturin` as build and deployment tool (already installed in the environment). To create bindings run `maturin develop`. To get max speed, compile using `maturin develop --release`. Once you defeated the rust compiler, your module we'll be available inside the environment.
